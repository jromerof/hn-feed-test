# Description

This is a simple app made with a server and a client. It queries the Hacker News api wiht a query for NodeJS news and updates the feed every hour.


This app is structured as it follows:
* Server: inside the server folder. Based on Nestjs.
* Client: inside the client folder. Based on Create React App.
* Database: a MongoDB image.

At the top level you will find the docker-compose file you need to run it.

# Instructions

Just run docker-compose build and then docker-compose up at the top folder level.
The default ports for the applications to run are 3000 for the server and 3001 for the client.
If you wish to change the ports where the apps are running you can modify the values of the .env file that is at the top level. The values are SERVER_PORT for the server port, and CLIENT_PORT for the client port.

Inside the database folder there is a .js script called mongo-init.js that makes an user and populates the database with a little bit of data, that way you can verify the client app works without having to wait for the Nestjs task to run.

