import Axios from "axios";
import { Dispatch, useState } from "react";
import { Article, ArticleReducerAction } from "../types";

function FeedRow({
  article,
  dispatchArticles,
}: {
  article: Article;
  dispatchArticles: Dispatch<ArticleReducerAction>;
}) {
  const [isMouseOver, setIsMouseOver] = useState(false);
  const [requestState, setRequestState] = useState("IDLE");

  const articleUrl = article?.story_url
    ? article.story_url
    : article?.["url"]
    ? article?.["url"]
    : "";

  const articleTitle = article?.story_title
    ? article.story_title
    : article?.title
    ? article.title
    : "-";

  function deleteStory() {
    setRequestState("LOADING");
    Axios.delete(`http://localhost:3000/articles/${article.story_id}`)
      .then(() => {
        dispatchArticles({ type: "DELETE", payload: article.story_id });
      })
      .catch((e) => {
        setRequestState("ERROR");
        console.error(e);
      });
  }

  return (
    <>
      {requestState === "ERROR" ? (
        <p>There was an error deleting this story. We're sorry</p>
      ) : null}
      <div
        onMouseEnter={() => setIsMouseOver(true)}
        onMouseLeave={() => setIsMouseOver(false)}
        className="grid news-row"
      >
        <div className="news-data">
          <a
            href={articleUrl}
            className={`${
              requestState === "LOADING" ? "no-events" : undefined
            }`}
          >
            <h1 className="news-title">{articleTitle}</h1>
            <p className="news-author">{` -${article.author}- `}</p>
          </a>
          <a
            href={articleUrl}
            className={`${
              requestState === "LOADING" ? "no-events" : undefined
            } grid`}
          >
            <p className="news-date">{parseArticleDate(article.created_at)}</p>
          </a>
        </div>
        <button
          title="Delete story from feed"
          className={`${isMouseOver ? `visible` : ""} delete-btn`}
          onClick={(e) => deleteStory()}
        >
          🗑️
        </button>
      </div>
    </>
  );
}

function parseArticleDate(date: string) {
  const today = new Date();
  const articleDate = new Date(date);
  const todayDateArray = today.toString().split(" ");
  const articleDateArray = articleDate.toString().split(" ");

  //Same Month and Year
  if (
    todayDateArray[1] === articleDateArray[1] &&
    todayDateArray[3] === articleDateArray[3]
  ) {
    if (todayDateArray[2] === articleDateArray[2]) {
      return articleDateArray[4];
    } else if (
      parseInt(todayDateArray[2]) - 1 ===
      parseInt(articleDateArray[2])
    ) {
      return "Yesterday";
    } else {
      return `${articleDateArray[1]} ${articleDateArray[2]}`;
    }
  } else {
    return `${articleDateArray[1]} ${articleDateArray[2]} ${articleDateArray[3]}`;
  }
}

export { FeedRow };
