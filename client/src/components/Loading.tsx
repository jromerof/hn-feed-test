export function Loading() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="171px"
      height="171px"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
    >
      <rect x="17.5" y="21" width="15" height="58" fill="#fc4309">
        <animate
          attributeName="y"
          repeatCount="indefinite"
          dur="1.4492753623188404s"
          calcMode="spline"
          keyTimes="0;0.5;1"
          values="-5.099999999999994;21;21"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-0.2898550724637681s"
        ></animate>
        <animate
          attributeName="height"
          repeatCount="indefinite"
          dur="1.4492753623188404s"
          calcMode="spline"
          keyTimes="0;0.5;1"
          values="110.19999999999999;58;58"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-0.2898550724637681s"
        ></animate>
      </rect>
      <rect x="42.5" y="21" width="15" height="58" fill="#ff765c">
        <animate
          attributeName="y"
          repeatCount="indefinite"
          dur="1.4492753623188404s"
          calcMode="spline"
          keyTimes="0;0.5;1"
          values="1.4250000000000043;21;21"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-0.14492753623188406s"
        ></animate>
        <animate
          attributeName="height"
          repeatCount="indefinite"
          dur="1.4492753623188404s"
          calcMode="spline"
          keyTimes="0;0.5;1"
          values="97.14999999999999;58;58"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-0.14492753623188406s"
        ></animate>
      </rect>
      <rect x="67.5" y="21" width="15" height="58" fill="#ffb646">
        <animate
          attributeName="y"
          repeatCount="indefinite"
          dur="1.4492753623188404s"
          calcMode="spline"
          keyTimes="0;0.5;1"
          values="1.4250000000000043;21;21"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1"
        ></animate>
        <animate
          attributeName="height"
          repeatCount="indefinite"
          dur="1.4492753623188404s"
          calcMode="spline"
          keyTimes="0;0.5;1"
          values="97.14999999999999;58;58"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1"
        ></animate>
      </rect>
    </svg>
  );
}
