import Axios from "axios";
import "./App.css";
import React, { useEffect, useReducer } from "react";
import { ArticleReducerAction, ArticleReducerState } from "./types";
import { FeedRow } from "./components/FeedRow";
import { Loading } from "./components/Loading";

function reduceArticles(
  state: ArticleReducerState,
  action: ArticleReducerAction
) {
  switch (action.type) {
    case "INIT":
      return { status: "DONE", articles: action.payload, error: null };
    case "DELETE": {
      const indexToDelete = state.articles.findIndex(
        (article) => article.story_id === action.payload
      );
      return {
        ...state,
        articles: [
          ...state.articles.slice(0, indexToDelete),
          ...state.articles.slice(indexToDelete + 1),
        ],
      };
    }
    case "ERROR":
      return {
        status: "ERROR",
        articles: [],
        error: action.payload,
      };

    default:
      return state;
  }
}

function App() {
  let [{ status, articles, error }, dispatchArticles] = useReducer(
    reduceArticles,
    {
      articles: [],
      status: "LOADING",
      error: null,
    }
  );

  useEffect(() => {
    Axios.get("http://localhost:3000/articles")
      .then((response) =>
        dispatchArticles({ type: "INIT", payload: response.data })
      )
      .catch((error) => {
        dispatchArticles({ type: "ERROR", payload: error });
      });
  }, []);

  return (
    <>
      <header className="px-4 bg-orange">
        <h1 className="text-white pb-4">HN Feed</h1>
        <p className="text-white fs-125">{`We <3 Hacker News!`}</p>
      </header>
      <main className="main-content">
        {status === "DONE" ? (
          articles.length > 0 ? (
            articles.map((article: any) => {
              return article?.story_title || article?.title ? (
                <FeedRow
                  article={article}
                  key={article.story_id}
                  dispatchArticles={dispatchArticles}
                />
              ) : null;
            })
          ) : (
            <p>Your feed is empty.</p>
          )
        ) : status === "LOADING" ? (
          <div className="center">
            <Loading />
          </div>
        ) : (
          <div className="center flex-column">
            <p className="pb-4">There was an error </p>
            <p>{error?.message}</p>
          </div>
        )}
      </main>
    </>
  );
}

export default App;
