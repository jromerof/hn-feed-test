export type Article = {
  created_at: string;
  title: string | null;
  url: string | null;
  author: string;
  points: number | null;
  story_text: string | null;
  comment_text: string;
  num_comments: number | null;
  story_id: number;
  story_title: string;
  story_url: string | null;
  parent_id: number | null;
  created_at_i: number;
  _tags: string[];
  objectID: string;
  _highlightResult: {
    author: { value: string; matchLevel: string; matchedWords: string[] };
    comment_text: {
      value: string;
      matchLevel: string;
      fullyHighlighted: boolean;
      matchedWords: string[];
    };
    story_title: {
      value: string;
      matchedWords: string[];
    };
    story_url: {
      value: string;
      matchLevel: string;
      matchedWords: string[];
    };
  };
};

export type ArticleReducerAction =
  | { type: "INIT"; payload: Array<Article> }
  | { type: "DELETE"; payload: number }
  | { type: "ERROR"; payload: Error };

export type ArticleReducerState = {
  articles: Article[];
  status: string;
  error: null | Error;
};
