db.createUser({
  user: "test",
  pwd: "1234",
  roles: [
    {
      role: "readWrite",
      db: "hnfeed",
    },
  ],
});

db.createCollection("articles");

db.articles.insertMany([
  {
    created_at: "2020-12-17T05:48:23.000Z",
    title: null,
    url: null,
    author: "bayindirh",
    points: null,
    story_text: null,
    comment_text:
      "Because a cluster doesn\u0026#x27;t work like a normal computer.\u003cp\u003eYour users don\u0026#x27;t see the nodes. They submit jobs and wait for their turn in the cluster. A sophisticated resource planner \u0026#x2F; job scheduler tries to empty the queue while optimizing job placement so the system usage can be maximized as much as possible.\u003cp\u003eAlso, users\u0026#x27; jobs work in under their own users. You need to isolate them. Giving them access to docker or any root level container engine is completely removing UNIX user security and isolation model and running in Windows95 mode. This also compromises system security since everyone is practically root at that point. Singularity is user-mode and its usage is increasing but then comes the next point.\u003cp\u003ePerformance and hardware access is critical in HPC. GPU and special HBAs like Infiniband requires direct access from processes to run at their maximum performance or work at all. GPU access is much more important than containerizing workloads. Docker GPU is here because nVidia wanted to containerize AI workloads on DGX\u0026#x2F;HGX systems. These technologies are maturing on HPC now.\u003cp\u003eIn performance front, consider the following: If main loop of your computation loses a second due to these abstractions, considering this loops run thousand times per core on many nodes, lost productivity is eye-watering. My simple application computes 1.7 \u003ci\u003emillion\u003c/i\u003e integrations per second per core. So, for working on long problems, increasing this number is critical.\u003cp\u003eLast but not the least, some of the applications run on these systems are developed for 20 years now. So, these applications are not some simple code bases which are extremely tidy and neat. You can\u0026#x27;t know\u0026#x2F;guess how these applications behave before running them inside a container. As I\u0026#x27;ve said, you need to be able to trust what you have too. So, we scientists and HPC administrators tend to walk slowly but surely.\u003cp\u003eDoing my job properly on the HPC side means my cluster works with utmost efficiency and bulletproof user isolation so people can trust the validity of their results and integrity of their privacy. Doing my job properly on the development side means that my code builds with minimum effort and with maximum performance on systems I support. HPC software is not a single service which works like a normal container workload. We need to evolve our software to run with minimum problems with containers and containers should evolve to accommodate our workloads, workflows and meet our other needs.\u003cp\u003eThe cutting edge technology doesn\u0026#x27;t solve every problem with same elegance. Also we\u0026#x27;re not a set of lazy academics or sysadmins just because our systems work more traditionally.",
    num_comments: null,
    story_id: 25445725,
    story_title: "Rocky Linux: A CentOS replacement by the CentOS founder",
    story_url: "https://github.com/rocky-linux/rocky",
    parent_id: 25450925,
    created_at_i: 1608184103,
    _tags: ["comment", "author_bayindirh", "story_25445725"],
    objectID: "25452948",
    _highlightResult: {
      author: {
        value: "bayindirh",
        matchLevel: "none",
        matchedWords: [],
      },
      comment_text: {
        value:
          "Because a cluster doesn't work like a normal computer.\u003cp\u003eYour users don't see the \u003cem\u003enodes\u003c/em\u003e. They submit jobs and wait for their turn in the cluster. A sophisticated resource planner / job scheduler tries to empty the queue while optimizing job placement so the system usage can be maximized as much as possible.\u003cp\u003eAlso, users' jobs work in under their own users. You need to isolate them. Giving them access to docker or any root level container engine is completely removing UNIX user security and isolation model and running in Windows95 mode. This also compromises system security since everyone is practically root at that point. Singularity is user-mode and its usage is increasing but then comes the next point.\u003cp\u003ePerformance and hardware access is critical in HPC. GPU and special HBAs like Infiniband requires direct access from processes to run at their maximum performance or work at all. GPU access is much more important than containerizing workloads. Docker GPU is here because nVidia wanted to containerize AI workloads on DGX/HGX systems. These technologies are maturing on HPC now.\u003cp\u003eIn performance front, consider the following: If main loop of your computation loses a second due to these abstractions, considering this loops run thousand times per core on many \u003cem\u003enodes\u003c/em\u003e, lost productivity is eye-watering. My simple application computes 1.7 \u003ci\u003emillion\u003c/i\u003e integrations per second per core. So, for working on long problems, increasing this number is critical.\u003cp\u003eLast but not the least, some of the applications run on these systems are developed for 20 years now. So, these applications are not some simple code bases which are extremely tidy and neat. You can't know/guess how these applications behave before running them inside a container. As I've said, you need to be able to trust what you have too. So, we scientists and HPC administrators tend to walk slowly but surely.\u003cp\u003eDoing my job properly on the HPC side means my cluster works with utmost efficiency and bulletproof user isolation so people can trust the validity of their results and integrity of their privacy. Doing my job properly on the development side means that my code builds with minimum effort and with maximum performance on systems I support. HPC software is not a single service which works like a normal container workload. We need to evolve our software to run with minimum problems with containers and containers should evolve to accommodate our workloads, workflows and meet our other needs.\u003cp\u003eThe cutting edge technology doesn't solve every problem with same elegance. Also we're not a set of lazy academics or sysadmins just because our systems work more traditionally.",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      story_title: {
        value: "Rocky Linux: A CentOS replacement by the CentOS founder",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value: "https://github.com/rocky-linux/rocky",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
  {
    created_at: "2020-12-17T05:44:46.000Z",
    title: null,
    url: null,
    author: "josephg",
    points: null,
    story_text: null,
    comment_text:
      "\u0026gt; you need to continue to learn outside of a school setting and cultivate that curiosity\u003cp\u003eThis is true of just about everyone in computing. This isn\u0026#x27;t an industry for people who expect to cruise on the knowledge they were given a decade ago. (Plenty of people do, but thats another rant.)\u003cp\u003eRegarding big-O, one habit \u0026#x2F; intuition I think its really important to develop is an intuition about how well something will actually perform in practice. Like, you should be able to guess within an order of magnitude or two:\u003cp\u003e- How many simple reads per second and writes per second your database can perform\u003cp\u003e- How many lookups \u0026#x2F; inserts per second to expect out of some standard data structures. (And how those numbers change as the collection grows)\u003cp\u003e- How many allocations your program does, and how much time is spent in the allocator\u003cp\u003e- About how large the steady state working memory size should be for your program\u003cp\u003eI\u0026#x27;m not talking about theoretical O(n) numbers. I mean, right now, if I write a tight loop in nodejs on my laptop writing random numbers to a JS Map(), how fast will it go? How does that compare to C\u0026#x2F;Rust? How many SET calls per second can a redis instance on my laptop handle? How about SELECT queries to postgres, or find({id:...}) calls to mongodb? Will the database be faster or slower than the nodejs program on my laptop thats issuing those queries? How many HTTP requests per second should I expect out of my express server? How many milliseconds should it take to statically render my web app to HTML? Etc.\u003cp\u003eAnd arguably \u003ci\u003emore\u003c/i\u003e importantly, it shouldn\u0026#x27;t take you more than ~20 minutes to go and find out the answer to any of those questions. Benchmarking is a delicate art, but if you can\u0026#x27;t measure, you programming blind.\u003cp\u003eI wrote a fuzzer the other day which did about 100 iterations \u0026#x2F; second. And I know something was wrong because the number was orders of magnitude lower than I intuitively expected. Some tweaking later its now running about 1000 iterations\u0026#x2F;second - still too slow. Profiling shows its now spending 99% of the time in a single function. I\u0026#x27;m hoping for another 10x when I rewrite that function. Without my intuition whispering in my ear, my program could have ended up 100x slower than it should be for no good reason.\u003cp\u003eAside: This might make a great interview question for a mid. \u0026quot;I have this simple piece of code. How fast do you think it will run on your laptop? Ok, share your screen and write a program to measure it. Talk to me about your answer!\u0026quot;",
    num_comments: null,
    story_id: 25445493,
    story_title: "A computer science study plan to become a software engineer",
    story_url: "https://github.com/jwasham/coding-interview-university",
    parent_id: 25452548,
    created_at_i: 1608183886,
    _tags: ["comment", "author_josephg", "story_25445493"],
    objectID: "25452928",
    _highlightResult: {
      author: {
        value: "josephg",
        matchLevel: "none",
        matchedWords: [],
      },
      comment_text: {
        value:
          "\u0026gt; you need to continue to learn outside of a school setting and cultivate that curiosity\u003cp\u003eThis is true of just about everyone in computing. This isn't an industry for people who expect to cruise on the knowledge they were given a decade ago. (Plenty of people do, but thats another rant.)\u003cp\u003eRegarding big-O, one habit / intuition I think its really important to develop is an intuition about how well something will actually perform in practice. Like, you should be able to guess within an order of magnitude or two:\u003cp\u003e- How many simple reads per second and writes per second your database can perform\u003cp\u003e- How many lookups / inserts per second to expect out of some standard data structures. (And how those numbers change as the collection grows)\u003cp\u003e- How many allocations your program does, and how much time is spent in the allocator\u003cp\u003e- About how large the steady state working memory size should be for your program\u003cp\u003eI'm not talking about theoretical O(n) numbers. I mean, right now, if I write a tight loop in \u003cem\u003enodejs\u003c/em\u003e on my laptop writing random numbers to a JS Map(), how fast will it go? How does that compare to C/Rust? How many SET calls per second can a redis instance on my laptop handle? How about SELECT queries to postgres, or find({id:...}) calls to mongodb? Will the database be faster or slower than the \u003cem\u003enodejs\u003c/em\u003e program on my laptop thats issuing those queries? How many HTTP requests per second should I expect out of my express server? How many milliseconds should it take to statically render my web app to HTML? Etc.\u003cp\u003eAnd arguably \u003ci\u003emore\u003c/i\u003e importantly, it shouldn't take you more than ~20 minutes to go and find out the answer to any of those questions. Benchmarking is a delicate art, but if you can't measure, you programming blind.\u003cp\u003eI wrote a fuzzer the other day which did about 100 iterations / second. And I know something was wrong because the number was orders of magnitude lower than I intuitively expected. Some tweaking later its now running about 1000 iterations/second - still too slow. Profiling shows its now spending 99% of the time in a single function. I'm hoping for another 10x when I rewrite that function. Without my intuition whispering in my ear, my program could have ended up 100x slower than it should be for no good reason.\u003cp\u003eAside: This might make a great interview question for a mid. \u0026quot;I have this simple piece of code. How fast do you think it will run on your laptop? Ok, share your screen and write a program to measure it. Talk to me about your answer!\u0026quot;",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      story_title: {
        value: "A computer science study plan to become a software engineer",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value: "https://github.com/jwasham/coding-interview-university",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
  {
    created_at: "2020-12-17T00:25:27.000Z",
    title: null,
    url: null,
    author: "manigandham",
    points: null,
    story_text: null,
    comment_text:
      "It\u0026#x27;s a PaaS service like Heroku and others. However it\u0026#x27;s the new generation of \u0026quot;serverless\u0026quot; billing models where you don\u0026#x27;t worry about instances\u0026#x2F;nodes\u0026#x2F;servers at all and pay in much finer-grained increments.\u003cp\u003eBasically node-js servers running your code with an integrated CDN, but they also support Go, Python and Ruby for running server-side logic.",
    num_comments: null,
    story_id: 25442072,
    story_title: "Vercel raise $40M Series B",
    story_url: "https://vercel.com/blog/series-b-40m-to-build-the-next-web",
    parent_id: 25442399,
    created_at_i: 1608164727,
    _tags: ["comment", "author_manigandham", "story_25442072"],
    objectID: "25450712",
    _highlightResult: {
      author: {
        value: "manigandham",
        matchLevel: "none",
        matchedWords: [],
      },
      comment_text: {
        value:
          "It's a PaaS service like Heroku and others. However it's the new generation of \u0026quot;serverless\u0026quot; billing models where you don't worry about instances/nodes/servers at all and pay in much finer-grained increments.\u003cp\u003eBasically \u003cem\u003enode-js\u003c/em\u003e servers running your code with an integrated CDN, but they also support Go, Python and Ruby for running server-side logic.",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      story_title: {
        value: "Vercel raise $40M Series B",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value: "https://vercel.com/blog/series-b-40m-to-build-the-next-web",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
  {
    created_at: "2020-12-16T22:55:24.000Z",
    title: null,
    url: null,
    author: "three_seagrass",
    points: null,
    story_text: null,
    comment_text:
      "\u0026gt;Block producers (miners) must find buyers for the blocks they produce, if they don\u0026#x27;t find buyers, they go bankrupt.\u003cp\u003eIn a buyers market.  With the small block size, bitcoin is a seller\u0026#x27;s (miner\u0026#x27;s) market and refusing to upgrade preserves their market power - hence the tragedy of the commons.\u003cp\u003e\u0026gt;You\u0026#x27;ve got it backwards, changes to Bitcoin need to be accepted by block consumers (node operators).\u003cp\u003eThe number of bitcoin nodes has been dropping for years.  The rational actors who are incentivized to support the network are making the decisions right now by choosing which forks to support.",
    num_comments: null,
    story_id: 25443489,
    story_title: "Bitcoin breaks above $20k",
    story_url:
      "https://www.nasdaq.com/articles/bitcoin-hits-record-above-%2420k-as-analysts-remain-confident-of-future-2020-12-16",
    parent_id: 25448561,
    created_at_i: 1608159324,
    _tags: ["comment", "author_three_seagrass", "story_25443489"],
    objectID: "25449912",
    _highlightResult: {
      author: {
        value: "three_seagrass",
        matchLevel: "none",
        matchedWords: [],
      },
      comment_text: {
        value:
          "\u0026gt;Block producers (miners) must find buyers for the blocks they produce, if they don't find buyers, they go bankrupt.\u003cp\u003eIn a buyers market.  With the small block size, bitcoin is a seller's (miner's) market and refusing to upgrade preserves their market power - hence the tragedy of the commons.\u003cp\u003e\u0026gt;You've got it backwards, changes to Bitcoin need to be accepted by block consumers (node operators).\u003cp\u003eThe number of bitcoin \u003cem\u003enodes\u003c/em\u003e has been dropping for years.  The rational actors who are incentivized to support the network are making the decisions right now by choosing which forks to support.",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      story_title: {
        value: "Bitcoin breaks above $20k",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value:
          "https://www.nasdaq.com/articles/bitcoin-hits-record-above-%2420k-as-analysts-remain-confident-of-future-2020-12-16",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
  {
    created_at: "2020-12-16T22:22:31.000Z",
    title: null,
    url: null,
    author: "aftbit",
    points: null,
    story_text: null,
    comment_text:
      "I\u0026#x27;ve had basically this exact setup running on a cloud server for the past few months. It\u0026#x27;s pretty liberating to be able to open ports to 10.44.0.0\u0026#x2F;24 and have all of my different machines easily access them, regardless of what networks they\u0026#x27;re connected to. I\u0026#x27;ve added in an instance of nsd that manages a zonefile so I can do:\u003cp\u003essh laptop.wg.mydomain.net\u003cp\u003ewhich resolves into 10.44.0.3 for example.\u003cp\u003eThere are two major downsides with this approach:\u003cp\u003e(1) all of your traffic must go by way of the bounce server, even if your machines are on the same LAN.\u003cp\u003e(2) the bounce host can observe all of your traffic.\u003cp\u003eI\u0026#x27;ve been considering adding a second layer of wireguard on top of this for end-to-end encryption between nodes. Each machine would have one interface configured like this article suggests simply to assign a stable NAT-traversing IP address. Then they\u0026#x27;d each have a second interface using the first-level IP addresses as endpoints. This would result in 2x the encryption overhead, both in CPU and more importantly in MTU, but it would mean the bounce host was no longer able to intercept even unencrypted data. If you run a daemon on all of the hosts, they can play with the endpoints used on this second level network to avoid using the bounce host when they find they can directly connect to each other, but the default path would work (if somewhat slowly) by the double tunnel process even without this daemon.\u003cp\u003eFor bonus points, run multiple bounce hosts, connect to all of them from each peer, then select the endpoint to use for each paired peer connection based on the total path latency.",
    num_comments: null,
    story_id: 25447805,
    story_title: "WireGuard Bounce Server Setup",
    story_url:
      "https://gitlab.com/ncmncm/wireguard-bounce-server/-/blob/master/README.md",
    parent_id: 25447805,
    created_at_i: 1608157351,
    _tags: ["comment", "author_aftbit", "story_25447805"],
    objectID: "25449596",
    _highlightResult: {
      author: {
        value: "aftbit",
        matchLevel: "none",
        matchedWords: [],
      },
      comment_text: {
        value:
          "I've had basically this exact setup running on a cloud server for the past few months. It's pretty liberating to be able to open ports to 10.44.0.0/24 and have all of my different machines easily access them, regardless of what networks they're connected to. I've added in an instance of nsd that manages a zonefile so I can do:\u003cp\u003essh laptop.wg.mydomain.net\u003cp\u003ewhich resolves into 10.44.0.3 for example.\u003cp\u003eThere are two major downsides with this approach:\u003cp\u003e(1) all of your traffic must go by way of the bounce server, even if your machines are on the same LAN.\u003cp\u003e(2) the bounce host can observe all of your traffic.\u003cp\u003eI've been considering adding a second layer of wireguard on top of this for end-to-end encryption between \u003cem\u003enodes\u003c/em\u003e. Each machine would have one interface configured like this article suggests simply to assign a stable NAT-traversing IP address. Then they'd each have a second interface using the first-level IP addresses as endpoints. This would result in 2x the encryption overhead, both in CPU and more importantly in MTU, but it would mean the bounce host was no longer able to intercept even unencrypted data. If you run a daemon on all of the hosts, they can play with the endpoints used on this second level network to avoid using the bounce host when they find they can directly connect to each other, but the default path would work (if somewhat slowly) by the double tunnel process even without this daemon.\u003cp\u003eFor bonus points, run multiple bounce hosts, connect to all of them from each peer, then select the endpoint to use for each paired peer connection based on the total path latency.",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      story_title: {
        value: "WireGuard Bounce Server Setup",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value:
          "https://gitlab.com/ncmncm/wireguard-bounce-server/-/blob/master/README.md",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
  {
    created_at: "2020-12-16T19:11:06.000Z",
    title: null,
    url: null,
    author: "nodesocket",
    points: null,
    story_text: null,
    comment_text: "USA!!! USA!!! USA!!!",
    num_comments: null,
    story_id: 25445669,
    story_title: "Jet Powered Volkswagen Beetle – $550k (sunnyvale)",
    story_url:
      "https://sfbay.craigslist.org/sby/cto/d/mountain-view-jet-powered-volkswagen/7241823593.html",
    parent_id: 25446914,
    created_at_i: 1608145866,
    _tags: ["comment", "author_nodesocket", "story_25445669"],
    objectID: "25447003",
    _highlightResult: {
      author: {
        value: "\u003cem\u003enodes\u003c/em\u003eocket",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      comment_text: {
        value: "USA!!! USA!!! USA!!!",
        matchLevel: "none",
        matchedWords: [],
      },
      story_title: {
        value: "Jet Powered Volkswagen Beetle – $550k (sunnyvale)",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value:
          "https://sfbay.craigslist.org/sby/cto/d/mountain-view-jet-powered-volkswagen/7241823593.html",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
  {
    created_at: "2020-12-16T19:03:27.000Z",
    title: null,
    url: null,
    author: "neeasade",
    points: null,
    story_text: null,
    comment_text:
      'I\u0026#x27;ve been a BSPWM user for about 5 years now.\u003cp\u003eIt\u0026#x27;s very comfy. The client is close to the shell, and you can have both automatic and manual tiling modes. When there is some feature you like that the author did not account for, you can query the state of the world in json and make decisions based on that.\u003cp\u003eA comment here mentions herbstluftwm, which came before BSPWM and I would even call a sister WM -- Baskerville even announced BSPWM on the Herbstluftwm mailing list when it was initially released[2]. I would say BSPWM has less to think about than in the other window manager (everything is a node vs climbing frames with nested layouts). It can be thought of as a kit for building what you want, even. For instance, I use a tag-based workflow[3], where I toggle the hidden flag of nodes, and keep only one desktop.\u003cp\u003eWRT the windows being kept as nodes in a binary tree, this is useful because you can then climb the tree and perform balances and rotations within a subselection of the whole thing. You can also use the tree datastructure to enforce layout, like here[1], a DWM-like stacking layout.\u003cp\u003eWith monocle mode, you can add fake padding for fullscreen windows, allowing you to have comfy fullscreen stuff that still accounts for a panel presence. With an external rules script, you can make decisions about what to do with windows as they appear, allowing dynamic layouts and decision making.\u003cp\u003eI\u0026#x27;m sure at the end of the day, you can get what you need with any window manager if you drill far enough (year of the linux desktop is here, if you are willing to read the source). BSPWM has been a nice base for flexible workflow types for me. I will also share screenshots I\u0026#x27;ve taken over the years here: \u003ca href="https:\u0026#x2F;\u0026#x2F;notes.neeasade.net\u0026#x2F;rice.html" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;notes.neeasade.net\u0026#x2F;rice.html\u003c/a\u003e (all bspwm past mid 2014).\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;old.reddit.com\u0026#x2F;r\u0026#x2F;bspwm\u0026#x2F;comments\u0026#x2F;euq5r7\u0026#x2F;a_dwmlike_stack_layout_script_for_bspwm\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;old.reddit.com\u0026#x2F;r\u0026#x2F;bspwm\u0026#x2F;comments\u0026#x2F;euq5r7\u0026#x2F;a_dwmlike_sta...\u003c/a\u003e\u003cp\u003e[2] \u003ca href="https:\u0026#x2F;\u0026#x2F;herbstluftwm.org\u0026#x2F;archive\u0026#x2F;msg00052.html" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;herbstluftwm.org\u0026#x2F;archive\u0026#x2F;msg00052.html\u003c/a\u003e\u003cp\u003e[3] \u003ca href="https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;neeasade\u0026#x2F;dotfiles\u0026#x2F;blob\u0026#x2F;master\u0026#x2F;bin\u0026#x2F;bin\u0026#x2F;btags" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;neeasade\u0026#x2F;dotfiles\u0026#x2F;blob\u0026#x2F;master\u0026#x2F;bin\u0026#x2F;bin\u0026#x2F;bta...\u003c/a\u003e',
    num_comments: null,
    story_id: 25441213,
    story_title:
      "bspwm: tiling window manager that represents windows as leaves of a binary tree",
    story_url: "https://github.com/baskerville/bspwm",
    parent_id: 25441213,
    created_at_i: 1608145407,
    _tags: ["comment", "author_neeasade", "story_25441213"],
    objectID: "25446875",
    _highlightResult: {
      author: {
        value: "neeasade",
        matchLevel: "none",
        matchedWords: [],
      },
      comment_text: {
        value:
          'I\'ve been a BSPWM user for about 5 years now.\u003cp\u003eIt\'s very comfy. The client is close to the shell, and you can have both automatic and manual tiling modes. When there is some feature you like that the author did not account for, you can query the state of the world in json and make decisions based on that.\u003cp\u003eA comment here mentions herbstluftwm, which came before BSPWM and I would even call a sister WM -- Baskerville even announced BSPWM on the Herbstluftwm mailing list when it was initially released[2]. I would say BSPWM has less to think about than in the other window manager (everything is a node vs climbing frames with nested layouts). It can be thought of as a kit for building what you want, even. For instance, I use a tag-based workflow[3], where I toggle the hidden flag of \u003cem\u003enodes\u003c/em\u003e, and keep only one desktop.\u003cp\u003eWRT the windows being kept as \u003cem\u003enodes\u003c/em\u003e in a binary tree, this is useful because you can then climb the tree and perform balances and rotations within a subselection of the whole thing. You can also use the tree datastructure to enforce layout, like here[1], a DWM-like stacking layout.\u003cp\u003eWith monocle mode, you can add fake padding for fullscreen windows, allowing you to have comfy fullscreen stuff that still accounts for a panel presence. With an external rules script, you can make decisions about what to do with windows as they appear, allowing dynamic layouts and decision making.\u003cp\u003eI\'m sure at the end of the day, you can get what you need with any window manager if you drill far enough (year of the linux desktop is here, if you are willing to read the source). BSPWM has been a nice base for flexible workflow types for me. I will also share screenshots I\'ve taken over the years here: \u003ca href="https://notes.neeasade.net/rice.html" rel="nofollow"\u003ehttps://notes.neeasade.net/rice.html\u003c/a\u003e (all bspwm past mid 2014).\u003cp\u003e[1] \u003ca href="https://old.reddit.com/r/bspwm/comments/euq5r7/a_dwmlike_stack_layout_script_for_bspwm/" rel="nofollow"\u003ehttps://old.reddit.com/r/bspwm/comments/euq5r7/a_dwmlike_sta...\u003c/a\u003e\u003cp\u003e[2] \u003ca href="https://herbstluftwm.org/archive/msg00052.html" rel="nofollow"\u003ehttps://herbstluftwm.org/archive/msg00052.html\u003c/a\u003e\u003cp\u003e[3] \u003ca href="https://github.com/neeasade/dotfiles/blob/master/bin/bin/btags" rel="nofollow"\u003ehttps://github.com/neeasade/dotfiles/blob/master/bin/bin/bta...\u003c/a\u003e',
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      story_title: {
        value:
          "bspwm: tiling window manager that represents windows as leaves of a binary tree",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value: "https://github.com/baskerville/bspwm",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
  {
    created_at: "2020-12-16T18:19:56.000Z",
    title: null,
    url: null,
    author: "nodesocket",
    points: null,
    story_text: null,
    comment_text:
      "I’m sorry I am not buying this story. No way his password was that and guarantee the presidents account has two-factor. Congratulations Victor on getting world wide press coverage but I am calling BS.",
    num_comments: null,
    story_id: 25445876,
    story_title: "Trump Twitter Was Hacked",
    story_url: "https://www.bbc.co.uk/news/technology-55337192",
    parent_id: 25446075,
    created_at_i: 1608142796,
    _tags: ["comment", "author_nodesocket", "story_25445876"],
    objectID: "25446126",
    _highlightResult: {
      author: {
        value: "\u003cem\u003enodes\u003c/em\u003eocket",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"],
      },
      comment_text: {
        value:
          "I’m sorry I am not buying this story. No way his password was that and guarantee the presidents account has two-factor. Congratulations Victor on getting world wide press coverage but I am calling BS.",
        matchLevel: "none",
        matchedWords: [],
      },
      story_title: {
        value: "Trump Twitter Was Hacked",
        matchLevel: "none",
        matchedWords: [],
      },
      story_url: {
        value: "https://www.bbc.co.uk/news/technology-55337192",
        matchLevel: "none",
        matchedWords: [],
      },
    },
  },
]);
