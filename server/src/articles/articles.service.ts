import { HttpService, Injectable, Param } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Article, FeedHit } from './entities/articles.entity';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Article.name) private readonly articleModel: Model<Article>,
    private httpService: HttpService,
  ) {}

  async findAll() {
    return await this.articleModel.find().sort({ created_at: -1 });
  }

  async remove(@Param('id') id: string) {
    return await this.articleModel.deleteOne({ story_id: id });
  }

  async addRecentFeed() {
    const feed: FeedHit = await (
      await this.httpService
        .get('https://hn.algolia.com/api/v1/search_by_date', {
          params: {
            query: 'nodejs',
          },
        })
        .toPromise()
    ).data;

    const insertResult = [];

    for await (const hit of feed.hits) {
      const res = await this.articleModel.findOneAndUpdate(
        { story_id: hit.story_id },
        hit,
        {
          rawResult: true,
          useFindAndModify: false,
          upsert: true,
        },
      );
      insertResult.push({
        ...res.lastErrorObject,
        id: hit.story_id,
      });
    }
    return insertResult;
  }
}
