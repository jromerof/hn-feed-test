import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

/*class HightlightResult {}

class Author {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

class CommentText {}

class StoryTitle {}

class StoryURL {}*/

@Schema()
export class Article extends mongoose.Document {
  @Prop()
  'created_at': string;
  @Prop()
  'title': string | null;
  @Prop()
  'url': string | null;
  @Prop()
  'author': string;
  @Prop()
  'points': number | null;
  @Prop()
  'story_text': string | null;
  @Prop()
  'comment_text': string;
  @Prop()
  'num_comments': number | null;
  @Prop()
  'story_id': number;
  @Prop()
  'story_title': string;
  @Prop()
  'story_url': string | null;
  @Prop()
  'parent_id': number | null;
  @Prop()
  'created_at_i': number;
  @Prop([String])
  '_tags': string[];
  @Prop()
  'objectID': string;
  @Prop({ type: mongoose.Document })
  '_highlightResult': {
    author: { value: string; matchLevel: string; matchedWords: string[] };
    comment_text: {
      value: string;
      matchLevel: string;
      fullyHighlighted: boolean;
      matchedWords: string[];
    };
    story_title: {
      value: string;
      matchedWords: string[];
    };
    story_url: {
      value: string;
      matchLevel: string;
      matchedWords: string[];
    };
  };
}

export class FeedHit {
  hits: Article[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
