import { HttpModule, HttpService } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Connection } from 'mongoose';
import { of } from 'rxjs';
import { ArticlesService } from './articles.service';

const mockRepository = {
  find: jest.fn().mockReturnThis(),
  sort: jest.fn(),
  deleteOne: jest.fn(),
  findOneAndUpdate: jest.fn(),
};

describe('ArticlesService', () => {
  let service: ArticlesService;
  let httpService: HttpService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesService,
        { provide: Connection, useValue: {} },
        { provide: getModelToken('Article'), useValue: mockRepository },
      ],
      imports: [HttpModule],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of articles', async () => {
      const expectedArticles = [];
      mockRepository.find().sort.mockReturnValue(expectedArticles);
      const articles = await service.findAll();
      expect(articles).toEqual(expectedArticles);
    });
  });

  describe('remove', () => {
    it('should return a delete query', async () => {
      const articleId = '1';
      const expectedDelete = {
        n: 1,
        ok: 1,
        deletedCount: 1,
      };

      mockRepository.deleteOne.mockReturnValue(expectedDelete);
      const articles = await service.remove(articleId);
      expect(articles).toEqual(expectedDelete);
    });
  });

  describe('addRecentFeed', () => {
    it('should an array of updated objects', async () => {
      const expectedUpdates = [];
      const result = {
        data: {
          hits: [],
        },
        status: 200,
        statusText: 'OK',
        headers: {},
        config: {},
      };
      jest.spyOn(httpService, 'get').mockImplementationOnce(() => of(result));
      mockRepository.findOneAndUpdate.mockReturnValue(expectedUpdates);
      const articles = await service.addRecentFeed();
      expect(articles).toEqual(expectedUpdates);
    });
  });
});
