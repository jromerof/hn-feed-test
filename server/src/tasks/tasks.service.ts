import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ArticlesService } from '../articles/articles.service';

@Injectable()
export class TasksService {
  constructor(private readonly articleService: ArticlesService) {}
  private readonly logger = new Logger(TasksService.name);
  @Cron('0 * * * *')
  async handleCron() {
    this.logger.debug(await this.articleService.addRecentFeed());
  }
}
