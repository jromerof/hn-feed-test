import { Module } from '@nestjs/common';
import { ArticlesModule } from 'src/articles/articles.module';
import { TasksService } from './tasks.service';

@Module({
  providers: [TasksService],
  imports: [ArticlesModule],
})
export class TasksModule {}
