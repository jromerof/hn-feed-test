FROM node:14.15.1-alpine as builder

LABEL maintainer="<j.romero@gmail.com>"

ENV NODE_ENV development

WORKDIR /usr/src/app

COPY package.json yarn.lock ./

RUN yarn install --only=build

COPY . .

RUN yarn build

FROM node:14.15.1-alpine AS production

ENV NODE_ENV production

WORKDIR /usr/src/app

COPY package.json yarn.lock ./

RUN yarn install --only=production

COPY . .

COPY --from=builder /usr/src/app/dist ./dist

CMD ["yarn", "start:prod"]
